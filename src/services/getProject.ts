import { JSDOM } from 'jsdom';

function getHitSetId(html: string) {
  const htmlMatch = html.match(/projects\/([A-Z0-9]+)\/tasks/);

  if (htmlMatch) {
    return htmlMatch[1];
  }

  return null;
}

function getRequesterId(html: string) {
  const htmlMatch = html.match(/requesters\/([A-Z0-9]+)\/projects/);

  if (htmlMatch) {
    return htmlMatch[1];
  }

  return null;
}

function getRequesterName(html:string, frag: DocumentFragment) {
  const mtsMatch = [...frag.querySelectorAll(`b`)].find((b) => b.textContent === `Requester:`);

  if (mtsMatch && mtsMatch.nextElementSibling && mtsMatch.nextElementSibling.textContent) {
    return mtsMatch.nextElementSibling.textContent.trim();
  }

  return null;
}

function getTitle(html:string, frag: DocumentFragment) {
  const mtsMatch = [...frag.querySelectorAll(`b`)].find((b) => b.textContent === `Title:`);

  if (mtsMatch && mtsMatch.nextElementSibling && mtsMatch.nextElementSibling.textContent) {
    return mtsMatch.nextElementSibling.textContent.trim();
  }

  return null;
}

function getDescription(html:string, frag: DocumentFragment) {
  const mtsMatch = [...frag.querySelectorAll(`b`)].find((b) => b.textContent === `Description:`);

  if (mtsMatch && mtsMatch.nextSibling && mtsMatch.nextSibling.nodeValue) {
    return mtsMatch.nextSibling.nodeValue.trim();
  }

  return null;
}

function getReward(html:string) {
  const htmlMatch = html.match(/Reward:.+?([0-9.]+)/);

  if (htmlMatch) {
    return Number(htmlMatch[1]);
  }

  return null;
}

function getProject(html:string) {
  const frag = JSDOM.fragment(html);

  return {
    hit_set_id: getHitSetId(html),
    requester_id: getRequesterId(html),
    requester_name: getRequesterName(html, frag),
    title: getTitle(html, frag),
    description: getDescription(html, frag),
    pay: getReward(html),
  };
}

module.exports = getProject;
