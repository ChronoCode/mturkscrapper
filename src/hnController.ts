const getProject = require(`./services/getProject`);
const { fetchPostsMtc } = require('./services/fetchPostsMtc');
const { Hit } = require('./entity/hit');
const axios = require('axios');
import { JSDOM } from 'jsdom';
const getRepository = require("typeorm").getRepository;
import * as hm from "./HitManager";
function isMasters(html: string) {
  const mtsMasters = html.match(/(Qualifications:).+(Masters Exists)/gi);
  const mtsPhoto = html.match(/(Qualifications:).+(Photo Moderation Masters Exists)/gi);
  return Boolean(mtsMasters || mtsPhoto);
}

function isUsOnly(html: string) {
  const mtsUs = html.match(/(Qualifications:).+(Location EqualTo US)/gi);
  const mtsUs2 = html.match(/(Qualifications:).+(Location In US)/gi);
  return Boolean(mtsUs || mtsUs2);
}

async function handleHit(html: string, url: string, posted: number) {
  const match = html.match(/projects\/([A-Z0-9]+)\/tasks/);
  if (!match) {
    return;
  }

  const hitSetId = match[1];
  let newUrl = "https://worker.mturk.com/projects/" + hitSetId + "/tasks/accept_random?format=json";
  let project = getProject(html);
  let hit = new Hit();
  hit.url = newUrl;
  hit.title = project.title;
  hit.hit_set_id = project.hit_set_id;
  hit.pay = project.pay;
  var hmf = await hm.HitManager.getInstance();

}

function handlePost(post: { message_html: any; post_date: number; }, url: string) {
  const frag = JSDOM.fragment(post.message_html);
  const mtsHits = frag.querySelectorAll(`.ctaBbcodeTable, .bbTable`);

  [...mtsHits].forEach((hit) => {
    const hasMturkLink = hit.querySelector(`a[href^="https://worker.mturk.com/"]`);

    if (hasMturkLink) {
      handleHit(hit.outerHTML, url, post.post_date * 1000);
    }
  });
}

async function fetchTVF() {
  try {
    const response = await axios.get(`https://forum.turkerview.com/hub.php?action=getPosts&order_by=post_date&limit=10`);
    return response.data.posts;
  }
  catch (error) {
    console.log(error);
  }
}

async function mturkcrowd() {
  const posts = await fetchPostsMtc();
  [...posts].reverse().forEach((post) => handlePost(post, `http://mturkcrowd.com/posts/${post.post_id}`));
}

async function turkerviewforum() {
  const posts = await fetchTVF();
  [...posts].reverse().forEach((post) => handlePost(post, `https://forum.turkerview.com/posts/${post.post_id}`));
}

//setInterval(mturkcrowd, delay);
//setInterval(turkerviewforum, delay);
function hnController() {
  return {
    mturkcrowd: mturkcrowd,
    turkerviewforum: turkerviewforum
  };
}

module.exports = hnController;