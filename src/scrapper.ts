/*'use strict';

const fetch = require('node-fetch');
const utils = require('utils');
const sleep = utils.promisify(setTimeout);

async function poll() {
    const res = await fetch('https://example.com/');
    const html = res.text();
    console.log(html);
    await sleep(3000);
    return poll();
}

poll().catch((error) => {
    console.error(error); // Handle errors
});*/

//'use strict';
//import * as HitManager from 
import { HitManager } from "./HitManager"; 
import * as nodeFetch from 'node-fetch';
import * as $ from 'cheerio';
const hn = require('./hnController');
const hitnote = new hn();
import * as Hit from "./entity/Hit";

//import {isResponse, convertUTCDate} from "./utils/utils";

async function getLatestThread() {
    const threadAddress = 'https://forum.turkerview.com/forums/daily-mturk-hits-threads.2/';
    let headers = { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36' };
    //{ agent:new HttpsProxyAgent('http://127.0.0.1:8580')}
    return nodeFetch.default(threadAddress, { headers: headers })
        .then(res => res.text())
        .then(async (html) => {
            let discussionListItem = $('.discussionListItem', html).not(".sticky").not(".locked");
            //let pageNav = discussionListItem.find(".itemPageNav");
            //if (pageNav.length > 0) {
            //    return pageNav.find("a")[0].attribs.href;
            // }
            //else {
            return discussionListItem.find('.titleText').find('.PreviewTooltip')[0].attribs.href;
            //}
        });
}


async function getLastPage() {
    async function asyncForEach(array: any[] | (typeof Hit.Hit)[], callback: { (hit: any): Promise<void>; (arg0: any, arg1: number, arg2: any): void; }) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    }

    
    let add = await getLatestThread();
    let threadAddress = 'https://forum.turkerview.com/' + add.substring(0, add.lastIndexOf('/')) + '/';
    let headers = { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36' };
    let threadAddy = "";
    let pageNum1 = 1;
    const HitManager1 = await HitManager.getInstance();

    return nodeFetch.default(threadAddress, { headers: headers })
        .then(res => res.text())
        .then((_html) => {
            poll(threadAddress, 1)
                .catch((error: any) => {
                    console.log(error); // Handle errors
                    poll();
                })
                .finally(poll)
        });

    async function poll(threadAddressInput?: string , pageNum?: number): Promise<void | Response> {
        let localHits:  Hit.Hit[] = [];
        
        if (!!threadAddressInput && !!pageNum) {
            threadAddy = threadAddressInput;
            pageNum1 = pageNum;
        }
        if (threadAddressInput != null && pageNum == null) {
            threadAddy = threadAddressInput;
            pageNum1 = 1;
        }

        let headers = { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36' };


        return nodeFetch.default(threadAddy + 'page-' + (pageNum1), { headers: headers })
            .then(res => res.text())
            .then(async (html) => {
                let res1 = $('.lockedAlert', html);
                if (typeof(res1) != undefined && res1.length > 0) {
                    let add1 = await getLatestThread();
                    let threadAddress = 'https://forum.turkerview.com/' + add1.substring(0, add1.lastIndexOf('/')) + '/';
                    poll(threadAddress, 1);
                }
                // console.log("New func call on page #" + pageNum1)
                // Get the number of the page we are on and the last page.
                try {
                    var results = $('.pageNavHeader', html);
                    if (results.length > 0) {
                        let numbers = $(results[0]).text().match(/\d+/g).map(Number);
                        if (numbers[0] != numbers[numbers.length - 1]) {
                            pageNum1++;
                        }
                    }

                }
                catch {
                    console.log("Error getting numbers from tukerform");
                }
                let msgs = $('.message', html)
                    .filter(function () {
                        let ONE_HOUR = 60 * 60 * 1000; /* ms */
                        let postedTime = new Date(parseInt($(this).find(".DateTime").attr("data-time")) * 1000);
                        return (postedTime > new Date((Date.now() - (ONE_HOUR + (ONE_HOUR / 2)))));
                    })
                    console.log("filtered messages size: " + msgs.length)
                let blockQuotes = $('blockquote', msgs);
                $(blockQuotes).each(function (_i, blockquoteElem) {
                    $('a', this).each(async function (_i, _urlElem) {
                        let link = $(this).attr('href');
                        // This is a PANDA contaning node
                        if (link.includes("accept_random")) {
                            let name;
                            let pay;
                            let url;
                            // Check if it's a hidden post (it has italics in the blockquote
                            // The title is itialics
                            let hiddenPost = $('i', blockquoteElem).filter(function (_i, _el) {
                                // this === el
                                if (this.children.length > 0)
                                    return this.children[0];
                            })
                            // It is a hidden post
                            if (hiddenPost.length > 0) {

                                // Check to see if it has a negetive review (thumbs-down) - If so, don't add to queue.
                                if ($('.fa-thumbs-o-down', blockquoteElem).length > 0)
                                    return;

                                // Get the name from the blockquote
                                var splits = $(hiddenPost).text().split('-');
                                pay = splits.pop().replace('$', '').trim();
                                name = splits.join('-');
                                // TODO: Read more fields from blockquoteELem
                            }
                            // Not a hiddenPost
                            else {
                                var t = $('b', this.parentNode).each(function (_i, _el) {
                                    if (!!this.children[0].data && this.children[0].data.includes("Title:")) {
                                        name = this.nextSibling.nextSibling.children[0].data;
                                    }
                                    if (!!this.children[0].data && this.children[0].data.includes("Reward:")) {
                                        pay = this.nextSibling.data.replace('$', '').trim();
                                    }
                                })
                                // TODO: Read more fields
                            }
                            var splits = link.split("/tasks")
                            var splits1 = splits[0].split("/");
                            var projectID = splits1[splits1.length - 1];
                            url = link;
                            let hit = new Hit.Hit();
                            hit.url = url;
                            hit.title = name;
                            hit.hit_set_id = projectID;
                            hit.pay = parseInt(pay);
                            localHits.push(hit);
                        }
                    });
                })
                
                let start = async () => {
                    await asyncForEach(localHits, async (hit: Hit.Hit) => {
                        await HitManager1.addToHits(hit);
                    });
                    // console.log('Done');
                }
                try {
                    console.log("Trying mturk crowd and turkerview")
                    hitnote.mturkcrowd();
                    hitnote.turkerviewforum();
                }
                catch (error) {
                    console.log(error);
                }


                if (localHits.length > 0)
                    await start();

                return new Promise((resolve) => {
                    setTimeout(resolve, 3000);
                });
            })
            .then(poll).catch((error) => {
                console.log(error);
                poll();
            });
    }
}

getLastPage();
