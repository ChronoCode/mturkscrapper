import * as puppeteer from "puppeteer"

export function convertUTCDate (date = new Date())  {
    return new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000)
}


export function isResponse(res: void | puppeteer.Response): res is puppeteer.Response {
    return (res as puppeteer.Response).text !== undefined;
}