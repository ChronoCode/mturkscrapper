import { Entity,  Column, Index, CreateDateColumn, PrimaryColumn, UpdateDateColumn, MoreThan, LessThan} from "typeorm";
import { format } from 'date-fns'

@Entity()
export class Hit {
    @Index()
    @PrimaryColumn()
    hit_set_id: string; 

    @Column()
    url: string;

    @Column()
    title: string;
    
    @Column({nullable: true})
    requester_id: string;

    @Column()
    pay: number;

    @Column({nullable: true})
    description: string;

    @Index()
    @Column({default: true})
    isQueued: boolean;

    @Column({default: false})
    checkedThisCycle: boolean;

    @Index()
    @CreateDateColumn()
    createdAtUtc: Date;

    @Index()
    @UpdateDateColumn()
    updatedAtUtc: Date;
}
    // TypeORM Query Operators
    export const MoreThanDate = (date: Date) => MoreThan(format(date, 'yyyy-MM-dd HH:mm:ss'))
    export const LessThanDate = (date: Date) => LessThan(format(date, 'yyyy-MM-dd HH:mm:ss'))
