import * as puppeteer from "puppeteer"
import * as CREDS from "./creds";
import {convertUTCDate} from "./utils/utils";
import { createConnection, getRepository, MoreThan } from "typeorm";
const format = require('date-fns').format;
import * as Hit from "./entity/Hit"; 

export class HitManager {
    browser: puppeteer.Browser;
    page: puppeteer.Page;
    page1: puppeteer.Page;
    private static _instance: HitManager;

    public static async getInstance()
    {
        if(!!this._instance) {
            return this._instance;
        }
        this._instance = new HitManager();
        // Do you need arguments? Make it a regular static method instead.
        await this._instance.init();
        return this._instance;
    }

    private constructor()
    {
    }

    async init() {
        createConnection();
        this.browser = await puppeteer.launch({
            headless: false
        });
        this.browser.wsEndpoint
        this.page = await this.browser.newPage();
        await this.logIn();
        this.page1 = await this.browser.newPage();
        await this.page1.setJavaScriptEnabled(false);
        try {
            this.processHitArray();
        }
        catch (error) {
            console.log(error);
        }
    }
    async processHitArray() {
        let repo = getRepository<Hit.Hit>("Hit");
        let ONE_HOUR = 60 * 60 * 1000; /* ms */
        let self = this;
        setTimeout(async function tick() {
            try {
                let queuedHits = await repo.find({
                    where: {
                        isQueued: true,
                        order: {
                            createdAtUtc: "ASC"
                        },
                        createdAtUtc: MoreThan(format(convertUTCDate(new Date((Date.now() - (ONE_HOUR + (ONE_HOUR / 2))))), 'yyyy-MM-dd HH:mm:ss')),
                        checkedThisCycle: false
                    }
                });
                if (queuedHits && queuedHits.length > 0) {
                    // Get the oldest updated time hit
                    const hit = queuedHits[0]; //.reduce((r, o) => o.createdAtUtc < r.createdAtUtc ? o : r);
                    if (!!hit) {
                        await self.pandaHit(hit).then((result) => {
                            if (!result) {
                                hit.checkedThisCycle = true;
                            }
                            else {
                                hit.isQueued = false;
                                console.log('Hit: ' + hit.title + " removed from queue.");
                            }
                            repo.save(hit);
                        })
                            .catch((error) => {
                                console.log(error);
                            });
                    }
                }
                else if (queuedHits && queuedHits.length == 0) {
                    let hitsToReset = await repo.find({
                        isQueued: true,
                        createdAtUtc: MoreThan(format(convertUTCDate(new Date((Date.now() - (ONE_HOUR + (ONE_HOUR / 2))))), 'yyyy-MM-dd HH:mm:ss')),
                        checkedThisCycle: true
                    });
                    hitsToReset.forEach(hit => hit.checkedThisCycle = false);
                    repo.save(hitsToReset);
                }
            }
            catch (error) {
                console.log("Error: " + error);
            }
            setTimeout(tick, 1000);
        }, 0);
    }
    async isHitInQueue(projectID: string) {
        function isResponse(res: void | puppeteer.Response): res is puppeteer.Response {
            return (res as puppeteer.Response).text !== undefined;
        }
        let text = "";
        let newPage = await this.browser.newPage();
        let response = await newPage.goto('https://worker.mturk.com/tasks.json', { waitUntil: 'domcontentloaded' }).catch(() => { });
        if (isResponse(response)) {
            text = await response.text();
        }
        else {
            return false;
        }

        newPage.close();
        let json = JSON.parse(text);
        if (!!json) {
            json.tasks.some((element: { project: { hit_set_id: string; }; }) => {
                if (element.project.hit_set_id === projectID) {
                    return true;
                }
                else {
                    return false;
                }
            });
            return false;
        }
    }
    async addToHits(hit: Hit.Hit) {
        let repo = getRepository<Hit.Hit>("Hit");
        try {
            await repo.insert(hit);
            console.log("New Hit added: " + hit.title);
        }
        catch{
            // If this fails, then it's an existing hit
        }

    }
    async logIn() {
        let page = this.page;
        await page.goto('https://worker.mturk.com/', { waitUntil: 'domcontentloaded' }).catch((error) => {
            console.error(error); // Handle errors
        });
        const USERNAME_SELECTOR = '#ap_email'; // "#login_field" is the copied value
        const PASSWORD_SELECTOR = '#ap_password';
        const BUTTON_SELECTOR = '#signInSubmit';
        let isLoggedIn = await page.evaluate(() => document.querySelector('#ap_email'));
        if (!!isLoggedIn) {
            await page.click(USERNAME_SELECTOR);
            await page.keyboard.type(CREDS.default.username);
            await page.click(PASSWORD_SELECTOR);
            await page.keyboard.type(CREDS.default.password);
            await page.click(BUTTON_SELECTOR);
            //await page.waitForNavigation({ waitUntil: 'domcontentloaded' });
        }
    }
    // Example hit
    // https://worker.mturk.com/projects/3KS2VSZD3GIECXCKZAOYVIGDDHWVHH/tasks.json
    async pandaHit(hit: Hit.Hit) {
        let page = this.page1;
        var splits = hit.url.split('tasks');
        var url = splits[0] + 'tasks/accept_random.json';
        try {
            let response = await page.goto(url, { timeout: 80000, waitUntil: 'domcontentloaded' });
            let text = await response.text();
            try {
                let json = JSON.parse(text);
                if (json.message.includes('Qualifications')) {
                    //   console.log('Qualifications');
                    return true;
                }
                else if (json.message.includes('There are no more of these HITs available.')) {
                    //    console.log("Going back to stack")
                    return false;
                }
                else {
                    // console.log("Too many requests at once, going back to stack.")
                    return false;
                }
            }
            catch {
                // If the json cant be parsed then it's a full HTML page, and the hit was successfully caught.
                // Play system beep to alert user to a good capture
                console.log('\x07' + " Hit caught: " + hit.title);
                return true;
            }
        }
        catch (e) {
            console.log("Timeout: " + e);
            return false;
        }
        /*Code used to query the dom before I found I can just return as sucessful if the JSON can't parse.
        
        let sucessfulPanda = await page.evaluate(() => document.querySelector('body > div.m-b-md > div.text-xs-right > nav > div > div > div > div > form > button'))
        if (!!sucessfulPanda) {
            console.log("Sucessfully grabbed hit");
            return true;
        }
        let errorMessage = await page.evaluate(() => document.querySelector('#MainContent > div:nth-child(2) > div > div > div > div.mturk-alert-content > h3').textContent);
        if (!!errorMessage && errorMessage.includes('Qualifications')) {
            console.log('Qualifications');
            return true;
        }
        console.log("Going back to stack");
        return false;*/
    }
}