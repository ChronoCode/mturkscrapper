import {MigrationInterface, QueryRunner} from "typeorm";

export class Initialize1569440495987 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP INDEX "IDX_2d6242dc1303f4ea45054ce0a9"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_d5db37970d111e9ae35431216b"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_4e9e2c346afd9b67f79d6da97f"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_c6f174ef094fe4af66c7d7a365"`, undefined);
        await queryRunner.query(`CREATE TABLE "temporary_hit" ("hit_set_id" varchar PRIMARY KEY NOT NULL, "url" varchar NOT NULL, "title" varchar NOT NULL, "requester_id" varchar, "pay" integer NOT NULL, "description" varchar, "isQueued" boolean NOT NULL, "createdAt" datetime NOT NULL DEFAULT (datetime('now')), "updatedAt" datetime NOT NULL DEFAULT (datetime('now')), "checkedThisCycle" boolean NOT NULL DEFAULT (0))`, undefined);
        await queryRunner.query(`INSERT INTO "temporary_hit"("hit_set_id", "url", "title", "requester_id", "pay", "description", "isQueued", "createdAt", "updatedAt") SELECT "hit_set_id", "url", "title", "requester_id", "pay", "description", "isQueued", "createdAt", "updatedAt" FROM "hit"`, undefined);
        await queryRunner.query(`DROP TABLE "hit"`, undefined);
        await queryRunner.query(`ALTER TABLE "temporary_hit" RENAME TO "hit"`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_2d6242dc1303f4ea45054ce0a9" ON "hit" ("updatedAt") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_d5db37970d111e9ae35431216b" ON "hit" ("createdAt") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_4e9e2c346afd9b67f79d6da97f" ON "hit" ("isQueued") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_c6f174ef094fe4af66c7d7a365" ON "hit" ("hit_set_id") `, undefined);
        await queryRunner.query(`DROP INDEX "IDX_2d6242dc1303f4ea45054ce0a9"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_d5db37970d111e9ae35431216b"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_4e9e2c346afd9b67f79d6da97f"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_c6f174ef094fe4af66c7d7a365"`, undefined);
        await queryRunner.query(`CREATE TABLE "temporary_hit" ("hit_set_id" varchar PRIMARY KEY NOT NULL, "url" varchar NOT NULL, "title" varchar NOT NULL, "requester_id" varchar, "pay" integer NOT NULL, "description" varchar, "isQueued" boolean NOT NULL DEFAULT (1), "createdAt" datetime NOT NULL DEFAULT (datetime('now')), "updatedAt" datetime NOT NULL DEFAULT (datetime('now')), "checkedThisCycle" boolean NOT NULL DEFAULT (0))`, undefined);
        await queryRunner.query(`INSERT INTO "temporary_hit"("hit_set_id", "url", "title", "requester_id", "pay", "description", "isQueued", "createdAt", "updatedAt", "checkedThisCycle") SELECT "hit_set_id", "url", "title", "requester_id", "pay", "description", "isQueued", "createdAt", "updatedAt", "checkedThisCycle" FROM "hit"`, undefined);
        await queryRunner.query(`DROP TABLE "hit"`, undefined);
        await queryRunner.query(`ALTER TABLE "temporary_hit" RENAME TO "hit"`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_2d6242dc1303f4ea45054ce0a9" ON "hit" ("updatedAt") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_d5db37970d111e9ae35431216b" ON "hit" ("createdAt") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_4e9e2c346afd9b67f79d6da97f" ON "hit" ("isQueued") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_c6f174ef094fe4af66c7d7a365" ON "hit" ("hit_set_id") `, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP INDEX "IDX_c6f174ef094fe4af66c7d7a365"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_4e9e2c346afd9b67f79d6da97f"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_d5db37970d111e9ae35431216b"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_2d6242dc1303f4ea45054ce0a9"`, undefined);
        await queryRunner.query(`ALTER TABLE "hit" RENAME TO "temporary_hit"`, undefined);
        await queryRunner.query(`CREATE TABLE "hit" ("hit_set_id" varchar PRIMARY KEY NOT NULL, "url" varchar NOT NULL, "title" varchar NOT NULL, "requester_id" varchar, "pay" integer NOT NULL, "description" varchar, "isQueued" boolean NOT NULL, "createdAt" datetime NOT NULL DEFAULT (datetime('now')), "updatedAt" datetime NOT NULL DEFAULT (datetime('now')), "checkedThisCycle" boolean NOT NULL DEFAULT (0))`, undefined);
        await queryRunner.query(`INSERT INTO "hit"("hit_set_id", "url", "title", "requester_id", "pay", "description", "isQueued", "createdAt", "updatedAt", "checkedThisCycle") SELECT "hit_set_id", "url", "title", "requester_id", "pay", "description", "isQueued", "createdAt", "updatedAt", "checkedThisCycle" FROM "temporary_hit"`, undefined);
        await queryRunner.query(`DROP TABLE "temporary_hit"`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_c6f174ef094fe4af66c7d7a365" ON "hit" ("hit_set_id") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_4e9e2c346afd9b67f79d6da97f" ON "hit" ("isQueued") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_d5db37970d111e9ae35431216b" ON "hit" ("createdAt") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_2d6242dc1303f4ea45054ce0a9" ON "hit" ("updatedAt") `, undefined);
        await queryRunner.query(`DROP INDEX "IDX_c6f174ef094fe4af66c7d7a365"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_4e9e2c346afd9b67f79d6da97f"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_d5db37970d111e9ae35431216b"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_2d6242dc1303f4ea45054ce0a9"`, undefined);
        await queryRunner.query(`ALTER TABLE "hit" RENAME TO "temporary_hit"`, undefined);
        await queryRunner.query(`CREATE TABLE "hit" ("hit_set_id" varchar PRIMARY KEY NOT NULL, "url" varchar NOT NULL, "title" varchar NOT NULL, "requester_id" varchar, "pay" integer NOT NULL, "description" varchar, "isQueued" boolean NOT NULL, "createdAt" datetime NOT NULL DEFAULT (datetime('now')), "updatedAt" datetime NOT NULL DEFAULT (datetime('now')))`, undefined);
        await queryRunner.query(`INSERT INTO "hit"("hit_set_id", "url", "title", "requester_id", "pay", "description", "isQueued", "createdAt", "updatedAt") SELECT "hit_set_id", "url", "title", "requester_id", "pay", "description", "isQueued", "createdAt", "updatedAt" FROM "temporary_hit"`, undefined);
        await queryRunner.query(`DROP TABLE "temporary_hit"`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_c6f174ef094fe4af66c7d7a365" ON "hit" ("hit_set_id") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_4e9e2c346afd9b67f79d6da97f" ON "hit" ("isQueued") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_d5db37970d111e9ae35431216b" ON "hit" ("createdAt") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_2d6242dc1303f4ea45054ce0a9" ON "hit" ("updatedAt") `, undefined);
    }

}
